import React, {useState} from 'react';
import './App.css';
import Button from 'react-bootstrap/Button';
function App() {

  const [click, setClick] = useState(false);
  const [isError, setIserror] = useState(false);

  const initResponse = {
    "context": {},
    "actions": {},
    "status": "",
    "payload": {
      "message": ""
    },
    "validation": {}
  }
  const [response, setresponse] = useState(initResponse);

  const headers = {
      'Content-Type': 'application/json',
      'Accept': '*/*',
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
  }
  const axios = require('axios').default;
  const api = "http://ab593cc3c417e4802bdcfcc640f77913-1846138369.ap-southeast-1.elb.amazonaws.com/api/v1/helloworld";
  const onClickButton = () => {
    setIserror(false);
    setClick(true);
    setresponse({
      ...response,
      "status": "Pending",
      "payload":{
        "message": "Request Send"
      }
    })
    axios.post(api, '', {headers})
    .then(function (response) {
      setresponse({
        ...response,
        "status": "Success",
        "payload":{
          "message":response.data.payload.message
        }
      }
      );
    })
    .catch(function (error) {
      setIserror(true);
      setresponse({
        ...response,
        "status": "Error",
        "payload":{
          "message": error.message
        }
      })
    });
  }
  return (
    <div className="wraper">
      <div className='header'>Symphony Connectivity Check</div>
      <div className='left'>
        <Button className='butn' size="lg" variant={click ? "success" : "primary"} onClick={onClickButton}>Zoral</Button>
      </div>
      <div className='right'>
      <br/>
        <div className='res'>
        {response.payload.message}
        </div>

      </div>
    </div>
  );
}

export default App;
